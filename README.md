dprustlib
-----------
dprustlib is an idiosyncratic, hodge-podge of rust code developed while learning 
rust. It is focused on finance topics, especially option pricing.

Licensed under  [GPL-3 or later](LICENSE.md)


### CHANGELOG

Please see the [CHANGELOG](CHANGELOG.md) for a release history.

