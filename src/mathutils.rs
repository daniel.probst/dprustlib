use std::f64::EPSILON;
use std::f64::NAN;

/// Numerical Recipes in C by Press, Teukolsky, Vetterling and Flannery. p301ff
pub fn root_brent(
    x1: f64,
    x2: f64,
    func: &dyn Fn(f64) -> f64,
    acceptable_err: f64,
    max_iterations: u32,
) -> Result<f64, f64> {
    let mut a = x1;
    let mut b = x2;
    let mut c = x2;

    let mut fa = func(a);
    let mut fb = func(b);
    if (fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0) {
        //return Err(NAN);
        panic!("crap");
    }

    let mut d = NAN;
    let mut e = NAN;
    let mut q;
    let mut r;
    let mut p;

    let mut fc = fb;
    for _ in 0..max_iterations {
        if (fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0) {
            // rename a, b, c and adjust bounding interval d
            c = a;
            fc = fa;
            d = b - a;
            e = d;
        }
        if fc.abs() < fb.abs() {
            a = b;
            b = c;
            c = a;
            fa = fb;
            fb = fc;
            fc = fa;
        }
        // convergence check
        let tol1 = 2.0 * EPSILON * b.abs() + 0.5 * acceptable_err;
        let xm = 0.5 * (c - b);
        if xm.abs() <= tol1 || fb == 0.0 {
            return Ok(b);
        }
        if e.abs() >= tol1 && fa.abs() > fb.abs() {
            // attempt inverse quadratic interpolation
            let s = fb / fa;
            if a == c {
                p = 2.0 * xm * s;
                q = 1.0 - s;
            } else {
                q = fa / fc;
                r = fb / fc;
                p = s * (2.0 * xm * q * (q - r) - (b - a) * (r - 1.0));
                q = (q - 1.0) * (r - 1.0) * (s - 1.0);
            }
            // check whether in bounds
            if p > 0.0 {
                q = -q;
            }
            p = p.abs();
            let min1 = 3.0 * xm * q * (tol1 - q).abs();
            let min2 = (e * q).abs();
            if 2.0 * p < min1.min(min2) {
                // accept interpolation
                e = d;
                d = p / q;
            } else {
                // interpolation failed, use bisection
                d = xm;
                e = d;
            }
        } else {
            // bounds decreasing too slowly, use bisection
            d = xm;
            e = d;
        }

        // move last best guess to a
        a = b;
        fa = fb;
        // evaluate new trial root
        if d.abs() > tol1 {
            b += d;
        } else {
            b += tol1.abs() * xm.signum();
        }
        fb = func(b);
    }
    Err(b)
}

/// Newton-Raphson unvariate root
///
/// An f64 version of the Newton-Raphson method for find a root of a univariate
/// function with a known derivative.
///
/// # Arguments
/// * `function` - a univariate function
/// * `derivative` - the derivative of the function
/// * `x0` - the initial guess
/// * `max_err` - the maximal error
/// * `max_iter` - the maximal number of iterations
///
/// # Return Value
/// * `Result(f64,f64)` - Either Ok(root) or Err(best guess)
///
/// # Examples
///  ```
/// use dprustlib::mathutils::root_nr;
///
/// // The function x^3 + x^2  - 4
/// fn f(x: f64) -> f64 {
///     x.powi(3) + x.powi(2) - 4.0
/// }
///
/// // The derivative of f (3x^2 + 2x)
/// fn fd(x: f64) -> f64 {
///     (3.0 * x.powi(2)) + (2.0 * x)
/// }
///
/// let result = root_nr(&f, &fd,
///                        10.0,   // Starting guess
///                        0.0001,     // Precision
///                        1000       // Iterations
///                       ).unwrap();
/// let actual = 1.31459; // The correct answer
///
/// let difference = (result - actual).abs();
///
/// assert!(difference < 0.0001);
/// ```
pub fn root_nr(
    function: &dyn Fn(f64) -> f64,
    derivative: &dyn Fn(f64) -> f64,
    x0: f64,
    max_err: f64,
    max_iter: u32,
) -> Result<f64, f64> {
    let mut current_x: f64 = x0;
    let mut next_x: f64;

    for _ in 0..max_iter {
        let deviation = function(current_x) / derivative(current_x);
        next_x = current_x - deviation;
        //dbg!(deviation);
        if deviation.abs() <= max_err {
            return Ok(next_x);
        }
        current_x = next_x;
        //dbg!(current_x);
    }
    return Err(current_x);
}

// Sample stats for monte carlo
//
/// sample statistics struct to manage  average and standard error of a sample of f64s
///
/// The struct SampleStats is  used to track number, sum and sum of squared samples to be able to
/// easily calculate the mean of a sample including it's standard error. The primary usage s for
/// convergence tracking in monte carlo simulations. The advantage of
/// keeping interim statistics in this way is the trivial aggregation of parallel simulations.
///
///
#[derive(Copy, Clone)]
pub struct SampleStats {
    /// the sample size
    pub n: usize,
    /// the sum of sample values
    pub sum: f64,
    /// the sum of squared sample values
    pub ss: f64,
}

impl std::fmt::Display for SampleStats {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "n:{}, sum:{}, sum_sq:{}", self.n, self.sum, self.ss,)
    }
}

impl std::fmt::Debug for SampleStats {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let (mean, var, sem) = self.eval();
        write!(
            f,
            "n:{}, sum:{}, sum_sq:{}, mean:{}, var:{}, stderr_mean:{}",
            self.n, self.sum, self.ss, mean, var, sem
        )
    }
}

impl SampleStats {
    /// instantiate a new and empty version of SampleStats
    pub fn new() -> SampleStats {
        SampleStats {
            n: 0,
            sum: 0.0,
            ss: 0.0,
        }
    }
    /// initialize a new SampleStats struct based on an initial observation
    pub fn init(sample: f64) -> SampleStats {
        SampleStats {
            n: 1,
            sum: sample,
            ss: sample * sample,
        }
    }
    ///  update the sample stats with one new observation
    pub fn update(&mut self, sample: f64) {
        self.n += 1;
        self.sum += sample;
        self.ss += sample * sample;
    }
    /// evaluate the SampleStat struct and return sample mean, sample variance and standard error
    /// (the standard deviation of the sample mean)
    pub fn eval(&self) -> (f64, f64, f64) {
        if self.n == 0 {
            panic!(
                "Cannot eval a SampleStruct with 0 samples. This function call should never occur."
            );
        }
        let avg = self.sum / self.n as f64;
        let var = self.ss / self.n as f64 - avg * avg;
        let sem = f64::sqrt(var / self.n as f64);
        (avg, var, sem)
    }
    /// merge two SampleStats structs, returning a new struct
    pub fn merge(s1: &SampleStats, s2: &SampleStats) -> SampleStats {
        SampleStats {
            n: s1.n + s2.n,
            sum: s1.sum + s2.sum,
            ss: s1.ss + s2.ss,
        }
    }
    /// update a SampleStats structs by adding the stats of another struct
    pub fn add(&mut self, s1: &SampleStats) {
        self.n += s1.n;
        self.sum += s1.sum;
        self.ss += s1.ss;
    }
}

// TESTS

#[cfg(test)]
mod tests {
    use crate::mathutils::root_nr;
    #[test]
    fn check_root_nr() {
        let tol_digits = 5;
        fn f(x: f64) -> f64 {
            x.powi(3) + x.powi(2) - 4.0
        }
        fn fd(x: f64) -> f64 {
            (3.0 * x.powi(2)) + (2.0 * x)
        }
        let result = root_nr(&f, &fd, 10.0, 0.0001, 1000).unwrap();
        let actual = 1.31459;
        let difference = (result - actual).abs();
        assert!(
            difference < f64::powi(10.0f64, -tol_digits),
            "result = {}, actual = {}",
            result,
            actual
        );
    }
}
