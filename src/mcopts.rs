use crate::mathutils::SampleStats;
use crate::quantopts::GBSMData;
use rand_distr::{Distribution, Normal};
use rayon::prelude::*;

#[derive(Clone)]
pub struct AsianGBM {
    pub settle: Vec<bool>,
    pub mu: Vec<f64>,
    pub sd: Vec<f64>,
    pub x: f64,
    pub pre: Option<(f64, usize)>,
}


impl std::fmt::Display for AsianGBM {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "settle:{:?}, mu:{:?}, sd:{:?}, x:{}, pre:{:?}",
            self.settle, self.mu, self.sd, self.x, self.pre,
        )
    }
}

impl AsianGBM {
    pub fn path_avg(&self) -> f64 {
        let n = self.settle.len();
        let mut csum_rnorm: f64 = 0.0; // cumulative sum of normals (this is in log(f) units)
        let mut csum_asset: f64 = 0.0; // cumulative sum of asset price
        let mut n_settle: usize = 0; // counter for number of settlments
        for i in 0..n {
            let normal = Normal::new(self.mu[i], self.sd[i]).unwrap();
            let rnorm = normal.sample(&mut rand::thread_rng());
            csum_rnorm += rnorm;
            if self.settle[i] {
                csum_asset += self.x * f64::exp(csum_rnorm);
                n_settle += 1;
            }
            //dbg!(i, rnorm, csum_rnorm, settle_vec[i], csum_asset);
        }
        // return cumulative sum and weight in term of #settlements
        match self.pre {
            Some((mean_pre, n_pre)) => {
                (csum_asset + mean_pre * n_pre as f64) / (n_settle + n_pre) as f64
            }
            None => csum_asset / n_settle as f64,
        }
    }
}

pub fn asian_mc(gbm: AsianGBM, gbsm_data: GBSMData, max_iter: usize, max_err: f64) -> SampleStats {
    // Run a first block until an option sample value is non-zero
    let mut sample_stats = SampleStats::new();
    while sample_stats.n <= max_iter {
        let sample_value = gbsm_data.intrinsic_value(gbm.path_avg());
        sample_stats.update(sample_value);
        if sample_stats.sum != 0.0 && sample_stats.n > 1 {
            break;
        }
    }
    // now we can iterate until precision is ok
    while sample_stats.eval().2 > max_err {
        //iter version
        let items = (0..1000000).into_par_iter();
        let res = items
            .map(|_x| gbsm_data.intrinsic_value(gbm.path_avg()))
            .map(|x| SampleStats::init(x))
            .reduce(
                || SampleStats::new(),
                |sum, item| SampleStats::merge(&sum, &item),
            );
        sample_stats.add(&res);
        //dbg!(sample_stats.eval());
        if sample_stats.n > max_iter {
            println!("Breaking on max_iter. Todo doc better.");
            break;
        }
    }
    sample_stats
}
