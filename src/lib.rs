#[cfg(test)]
#[macro_use]
extern crate approx;

/// math utilities: sample statistics, root finder
pub mod mathutils;
/// monte carlo option pricing
pub mod mcopts;
/// Module of typical functions used for option pricing: generalized Black Scholes Merton, asian
/// options, implied volatiltiy etc.
pub mod quantopts;
/// financial instrument tenors
pub mod tenors;
