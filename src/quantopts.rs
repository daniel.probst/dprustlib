use crate::mathutils::{root_brent, root_nr};
use rand_distr::Distribution;
use serde::Deserialize;
use statrs::distribution::{Continuous, ContinuousCDF, Normal};
use std::f64::consts::PI;

/// The type of an option, either `Put` or `Call`.
#[derive(Copy, Clone, Debug, Deserialize)]
pub enum OptionType {
    Put,
    Call,
}

impl std::fmt::Display for OptionType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            OptionType::Put => write!(f, "Put"),
            OptionType::Call => write!(f, "Call"),
        }
    }
}

/// Struture containing market data for generalized Black Scholes Merton style models
///
/// This is mainly used to describe a market quote and then deduce implied volatility.
/// Fields are the same as for the struct GBSMdata, except for price vs volatility.
#[derive(Copy, Clone, Debug, Deserialize)]
pub struct GBSMMarketData {
    /// a variant of the OptionType enum specifying either put or call type
    pub option_type: OptionType,
    /// the price of the underlying asset. Typically the current spot price or
    /// the futures price in case of options on futures
    pub asset_price: f64,
    /// the strike price
    pub strike: f64,
    /// the time to maturity (typically in fractions of a year, e.g.  0.5 is half a year)
    pub time: f64,
    /// the continuous-time risk-free interest rate
    pub r: f64,
    /// the cost of carry:
    /// * `b=r` - gives the Black-Scholes (1973) stock option model
    /// * `b=r-q` - gives the Merton (1973) stock option model with continuous dividend yield `q`
    /// * `b=0` - gives the Black (1976) futures option model
    /// * `b=r=0`  - gives the Asay (1982) margined futures option model
    /// * `b=r-rf` - gives the Garman and Kohlhagen (1983) currency option model
    pub b: f64,
    /// the price of the option
    pub price: f64,
}

impl std::fmt::Display for GBSMMarketData {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "OptionType:{}, asset price:{}, strike:{}, time:{}, interest:{}, carry:{}, price:{}",
            self.option_type, self.asset_price, self.strike, self.time, self.r, self.b, self.price
        )
    }
}

impl GBSMMarketData {
    /// calculate the implied volatility
    ///
    /// # Return Value
    /// * `f64` - the implied volatility
    /// # Examples
    /// ```
    /// use dprustlib::quantopts::{GBSMMarketData, OptionType};
    /// let optdat: GBSMMarketData = GBSMMarketData {
    ///         option_type: OptionType::Call,
    ///         asset_price: 100.0,
    ///         strike: 100.0,
    ///         r: 0.01,
    ///         b: 0.0,
    ///         time: 1.0,
    ///         price: 10.0,
    /// };
    /// println!("Implied volatility: {:?}", optdat.gbsm_ivol(0.001,10000));
    /// ```
    pub fn gbsm_ivol(&self, acceptable_err: f64, max_iterations: u32) -> Result<f64, f64> {
        gbsm_ivol(
            self.price,
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            acceptable_err,
            max_iterations,
        )
    }
}

/// Struture containing parameters needed for pricing generalized Black Scholes Merton style models
#[derive(Copy, Clone, Deserialize)]
pub struct GBSMData {
    /// a variant of the OptionType enum specifying either put or call type
    pub option_type: OptionType,
    /// the price of the underlying asset. Typically the current spot price or
    /// the futures price in case of options on futures
    pub asset_price: f64,
    /// the strike price
    pub strike: f64,
    /// the time to maturity (typically in fractions of a year, e.g.  0.5 is half a year)
    pub time: f64,
    /// the continuous-time risk-free interest rate
    pub r: f64,
    /// the cost of carry:
    /// * `b=r` - gives the Black-Scholes (1973) stock option model
    /// * `b=r-q` - gives the Merton (1973) stock option model with continuous dividend yield `q`
    /// * `b=0` - gives the Black (1976) futures option model
    /// * `b=r=0`  - gives the Asay (1982) margined futures option model
    /// * `b=r-rf` - gives the Garman and Kohlhagen (1983) currency option model
    pub b: f64,
    /// the volatility (in terms of 0.1 for 10% per time unit)
    pub sigma: f64,
}

impl std::fmt::Display for GBSMData {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "OptionType:{}, asset price:{}, strike:{}, time:{}, interest:{}, carry:{}, volatility:{}",
            self.option_type, self.asset_price, self.strike, self.time, self.r, self.b, self.sigma
        )
    }
}

impl GBSMData {
    /// calculate the intrinsic value given a settlement value
    pub fn intrinsic_value(&self, settle: f64) -> f64 {
        match self.option_type {
            OptionType::Call => f64::exp(-self.r * self.time) * f64::max(settle - self.strike, 0.0),
            OptionType::Put => f64::exp(-self.r * self.time) * f64::max(self.strike - settle, 0.0),
        }
    }
    /// calculate the option price
    ///
    /// # Return Value
    /// * `f64` - the option price
    /// # Examples
    /// ```
    /// use dprustlib::quantopts::{GBSMData, OptionType};
    /// let optdat: GBSMData = GBSMData {
    ///         option_type: OptionType::Call,
    ///         asset_price: 100.0,
    ///         strike: 100.0,
    ///         r: 0.01,
    ///         b: 0.0,
    ///         time: 1.0,
    ///         sigma: 0.3,
    /// };
    /// println!("Premium: {}", optdat.gbsm_option());
    /// ```
    pub fn gbsm_option(&self) -> f64 {
        gbsm_option(
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            self.sigma,
        )
    }
    // the delta of the option
    pub fn gbsm_delta(&self) -> f64 {
        gbsm_delta(
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            self.sigma,
        )
    }
    pub fn gbsm_strikedelta(&self) -> f64 {
        gbsm_strikedelta(
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            self.sigma,
        )
    }
    // the gamma of the option
    pub fn gbsm_gamma(&self) -> f64 {
        gbsm_gamma(
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            self.sigma,
        )
    }
    // the theta of the option
    pub fn gbsm_theta(&self) -> f64 {
        gbsm_theta(
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            self.sigma,
        )
    }
    // the vega of the option
    pub fn gbsm_vega(&self) -> f64 {
        gbsm_vega(
            self.option_type,
            self.asset_price,
            self.strike,
            self.time,
            self.r,
            self.b,
            self.sigma,
        )
    }
    /// the intrinsic value given a random sample for the
    /// lognormal distribution of the terminal vlaue
    pub fn random_sample(&self) -> f64 {
        let mu = (self.b - 0.5 * self.sigma * self.sigma) * self.time;
        let sd = self.sigma * self.time.sqrt();
        let normal = rand_distr::Normal::new(mu, sd).unwrap();
        let rnorm = normal.sample(&mut rand::thread_rng());
        self.intrinsic_value(self.asset_price * f64::exp(rnorm))
    }
}

/// generalized Black-Scholes-Merton option price
///
/// The option price assuming the underlying process is a geometric brownian motion. The
/// formula is taken from Haug (2007) *Option Pricing Formulas*.  
///
/// # Arguments
/// * `option_type` - a variant of the OptionType enum specifying either put or call type
/// * `asset_price` - the price of the underlying asset. Typically the current spot price
///  or the futures price in case of options on futures
/// * `strike` - the strike price
/// * `time` - the time to maturity (typically in fractions of year, e.g. 0.5 for half a year)
/// * `r` - the continuous time risk-free interest rate
/// * `b` - the cost of carry (see below)
/// * `sigma` - the volatility (in terms of 0.1 for 10% per time unit)
///
/// # Cost of carry and different models
/// * `b=r` - gives the Black-Scholes (1973) stock option model
/// * `b=r-q` - gives the Merton (1973) stock option model with continuous dividend yield `q`
/// * `b=0` - gives the Black (1976) futures option model
/// * `b=r=0`  - gives the Asay (1982) margined futures option model
/// * `b=r-rf` - gives the Garman and Kohlhagen (1983) currency option model
///
/// # Return Value
/// * `f64` - the option price
/// # Examples
/// ```
/// use dprustlib::quantopts::{gbsm_option,OptionType};
/// println!("Call(asset_type=100,strike=100,time=1,r=0.01,b=0,sigma=0.3) = {}",
///     gbsm_option(OptionType::Call, 100.0, 100.0, 1.0, 0.01, 0.0, 0.3));
/// ```
pub fn gbsm_option(
    option_type: OptionType,
    asset_price: f64,
    strike: f64,
    time: f64,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let n = Normal::new(0.0, 1.0).unwrap();
    let d1 = (f64::ln(asset_price / strike) + (b + sigma * sigma / 2.0) * time)
        / (sigma * f64::sqrt(time));
    let d2 = d1 - sigma * f64::sqrt(time);
    match option_type {
        OptionType::Call => {
            asset_price * ((b - r) * time).exp() * n.cdf(d1)
                - strike * (-r * time).exp() * n.cdf(d2)
        }
        OptionType::Put => {
            strike * (-r * time).exp() * n.cdf(-d2)
                - asset_price * ((b - r) * time).exp() * n.cdf(-d1)
        }
    }
}
/// generalized Black-Scholes-Merton delta
///
/// The derivative of the option value with respect to the asset price.
/// The formula is taken from Haug (2007) *Option Pricing Formulas*.  
///
/// # Arguments
/// * `option_type` - a variant of the OptionType enum specifying either put or call type
/// * `asset_price` - the price of the underlying asset. Typically the current spot price
///  or the futures price in case of options on futures
/// * `strike` - the strike price
/// * `time` - the time to maturity (typically in fractions of year, e.g. 0.5 for hlaf a year)
/// * `r` - the continuous time risk-free interest rate
/// * `b` - the cost of carry (see below)
/// * `sigma` - the volatility (in terms of 0.1 for 10% per time unit)
///
/// # Cost of carry and different models
/// * `b=r` - gives the Black-Scholes (1973) stock option model
/// * `b=r-q` - gives the Merton (1973) stock option model with continuous dividend yield `q`
/// * `b=0` - gives the Black (1976) futures option model
/// * `b=r=0`  - gives the Asay (1982) margined futures option model
/// * `b=r-rf` - gives the Garman and Kohlhagen (1983) currency option model
///
/// # Return Value
/// * `f64` - the delta
pub fn gbsm_delta(
    option_type: OptionType,
    asset_price: f64,
    strike: f64,
    time: f64,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let n = Normal::new(0.0, 1.0).unwrap();
    let d1 = (f64::ln(asset_price / strike) + (b + sigma * sigma / 2.0) * time)
        / (sigma * f64::sqrt(time));
    match option_type {
        OptionType::Call => f64::exp((b - r) * time) * n.cdf(d1),
        OptionType::Put => f64::exp((b - r) * time) * (n.cdf(d1) - 1.0),
    }
}
pub fn gbsm_strikedelta(
    option_type: OptionType,
    asset_price: f64,
    strike: f64,
    time: f64,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let n = Normal::new(0.0, 1.0).unwrap();
    let d1 = (f64::ln(asset_price / strike) + (b + sigma * sigma / 2.0) * time)
        / (sigma * f64::sqrt(time));
    let d2 = d1 - sigma * f64::sqrt(time);
    match option_type {
        OptionType::Call => f64::exp((-r) * time) * n.cdf(d2),
        OptionType::Put => f64::exp((-r) * time) * n.cdf(-d2),
    }
}
pub fn gbsm_gamma(
    _option_type: OptionType,
    s: f64,
    x: f64,
    time: f64,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let n = Normal::new(0.0, 1.0).unwrap();
    let d1 = (f64::ln(s / x) + (b + sigma * sigma / 2.0) * time) / (sigma * f64::sqrt(time));
    f64::exp((b - r) * time) * n.pdf(d1) / (s * sigma * f64::sqrt(time))
}

// TODO test
pub fn gbsm_theta(
    option_type: OptionType,
    s: f64,
    x: f64,
    time: f64,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let n = Normal::new(0.0, 1.0).unwrap();
    let d1 = (f64::ln(s / x) + (b + sigma * sigma / 2.0) * time) / (sigma * f64::sqrt(time));
    let d2 = d1 - sigma * f64::sqrt(time);
    let theta1 = -(s * f64::exp((b - r) * time) * n.pdf(d1) * sigma) / (2.0 * f64::sqrt(time));
    match option_type {
        OptionType::Call => {
            theta1
                - (b - r) * s * f64::exp((b - r) * time) * n.cdf(d1)
                - r * x * f64::exp(-r * time) * n.cdf(d2)
        }
        OptionType::Put => {
            theta1
                + (b - r) * s * f64::exp((b - r) * time) * n.cdf(-d1)
                + r * x * f64::exp(-r * time) * n.cdf(-d2)
        }
    }
}
pub fn gbsm_vega(
    _option_type: OptionType,
    s: f64,
    x: f64,
    time: f64,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let n = Normal::new(0.0, 1.0).unwrap();
    let d1 = (f64::ln(s / x) + (b + sigma * sigma / 2.0) * time) / (sigma * f64::sqrt(time));
    s * f64::exp((b - r) * time) * n.pdf(d1) * f64::sqrt(time)
}

/// call premium derived from put premium
///
/// use put-call parity for GBSM to calculate the call premium given the put premium
pub fn gbsm_call_from_put(
    put_premium: f64,
    asset_price: f64,
    strike: f64,
    time: f64,
    r: f64,
    b: f64,
) -> f64 {
    put_premium - f64::exp(-r * time) * strike + f64::exp((b - r) * time) * asset_price
}

/// put premium derived from call premium
///
/// use put-call parity for GBSM to calculate the put premium given the call premium
pub fn gbsm_put_from_call(
    call_premium: f64,
    asset_price: f64,
    strike: f64,
    time: f64,
    r: f64,
    b: f64,
) -> f64 {
    call_premium + f64::exp(-r * time) * strike - f64::exp((b - r) * time) * asset_price
}

/// Corrado Miller approximate implied volatility
///
/// Calculate an quadratic approximation to implied volatility from premium. Follows
/// C.J. Corrado, T. W. Miller, Jr./Journal of Banking & Finance 20 (1996) 595-603.
/// p559 eq. (10)
///
pub fn gbsm_cw_approx_vol(
    mut premium: f64,
    option_type: OptionType,
    asset_price: f64,
    strike: f64,
    time: f64,
    r: f64,
    b: f64,
) -> f64 {
    match option_type {
        OptionType::Put => {
            premium = gbsm_call_from_put(premium, asset_price, strike, time, r, b);
        }
        OptionType::Call => (),
    }
    let s = asset_price * f64::exp((b - r) * time);
    let x = strike * f64::exp((-r) * time);
    f64::sqrt(2.0 * PI / time) / (s + x)
        * (premium - (s - x) / 2.0
            + f64::sqrt((premium - (s - x) / 2.0).powi(2) - (s - x).powi(2) / PI))
}

// TODO refactor this for a sane error message
pub fn gbsm_ivol(
    premium: f64,
    option_type: OptionType,
    s: f64,
    x: f64,
    time: f64,
    r: f64,
    b: f64,
    acceptable_err: f64,
    max_iterations: u32,
) -> Result<f64, f64> {
    root_nr(
        &|sigma: f64| gbsm_option(option_type, s, x, time, r, b, sigma) - premium,
        &|sigma: f64| gbsm_vega(option_type, s, x, time, r, b, sigma),
        0.5,
        // TODO refactor initial  guess
        acceptable_err,
        max_iterations,
    )
}

pub fn hhm_option(
    option_type: OptionType,
    s: f64,
    sa: f64,
    mut x: f64,
    t1: f64,
    t: f64,
    n: u32,
    m: i32,
    r: f64,
    b: f64,
    sigma: f64,
) -> f64 {
    let norm = Normal::new(0.0, 1.0).unwrap();
    // averaging period is from t to t1 in n steps
    if n < 2 {
        panic!("Must have more than 1 averaging periods.");
    } //refactor to error
    let nf: f64 = n as f64;
    let mf: f64 = m as f64;
    let h = (t - t1) / (nf - 1.0);
    let ea: f64;
    if b.abs() < 1.0e-6 {
        ea = s;
    } else {
        ea = s / nf * f64::exp(b * t1) * (1.0f64 - f64::exp(b * h * nf))
            / (1.0f64 - f64::exp(b * h));
    }
    if m > 0 && sa > nf / mf * x {
        match option_type {
            OptionType::Put => return 0.0f64,
            OptionType::Call => {
                let sa = sa * mf / nf + ea * (nf - mf) / nf;
                return (sa - x) * f64::exp(-r * t);
            }
        }
    };
    if (mf - nf + 1.0).abs() < 1e-6 {
        x = nf * x - (nf - 1.0) * sa;
        return 1.0f64 / nf * gbsm_option(option_type, s, x, t, r, b, sigma);
    }
    let ea2: f64;
    if b.abs() < 1.0e-6 {
        ea2 = s * s * f64::exp(sigma * sigma * t1) / (nf * nf)
            * ((1.0f64 - f64::exp(sigma * sigma * h * nf)) / (1.0 - f64::exp(sigma * sigma * h))
                + 2.0f64 / (1.0f64 - f64::exp(sigma * sigma * h))
                    * (nf
                        - (1.0f64 - f64::exp(sigma * sigma * h * nf))
                            / (1.0f64 - f64::exp(sigma * sigma * h))));
    } else {
        ea2 = s * s * f64::exp((2.0f64 * b + sigma * sigma) * t1) / (nf * nf)
            * ((1.0f64 - f64::exp((2.0f64 * b + sigma * sigma) * h * nf))
                / (1.0f64 - f64::exp((2.0f64 * b + sigma * sigma) * h))
                + 2.0f64 / (1.0f64 - f64::exp((b + sigma * sigma) * h))
                    * ((1.0f64 - f64::exp(b * h * nf)) / (1.0f64 - f64::exp(b * h))
                        - (1.0f64 - f64::exp((2.0f64 * b + sigma * sigma) * h * nf))
                            / (1.0f64 - f64::exp((2.0f64 * b + sigma * sigma) * h))));
    }
    let sigmaa = f64::sqrt((f64::ln(ea2) - 2.0f64 * f64::ln(ea)) / t);
    let ov: f64;
    if m > 0 {
        x = nf / (nf - mf) * x - mf / (nf - mf) * sa;
    }
    let d1 = (f64::ln(ea / x) + sigmaa * sigmaa / 2.0f64 * t) / (sigmaa * f64::sqrt(t));
    let d2 = d1 - sigmaa * f64::sqrt(t);
    match option_type {
        OptionType::Call => {
            ov = f64::exp(-r * t) * (ea * norm.cdf(d1) - x * norm.cdf(d2));
        }
        OptionType::Put => {
            ov = f64::exp(-r * t) * (x * norm.cdf(-d2) - ea * norm.cdf(-d1));
        }
    };
    ov * (nf - mf) / nf
}
/// Turnbull Wakeman asian option price
///
/// The option price of an average price option assuming the underlying process is a geometric brownian motion. The
/// formula is taken from Haug (2007) *Option Pricing Formulas* 189ff.  
///
/// # Arguments
/// * `option_type` - a variant of the OptionType enum specifiy put or call type
/// * `s` - the price of the underlying asset
/// * `x` - the strike price
/// * `time` - the time to the end of the averaging period
/// * `tau` - the time to the beginning of the averaging period
/// * `r` - the interest rate
/// * `b` - the cost of carry (see below)
/// * `sigma` - the volatility (in terms of 0.1 for 10% per time unit)
///
/// # Cost of carry and different models
/// * `b=r` - gives the Black-Scholes (1973) stock option model
/// * `b=r-q` - gives the Merton (1973) stock option model with continuous dividend yield `q`
/// * `b=0` - gives the Black (1976) futures option model
/// * `b=r=0`  - gives the Asay (1982) margined futures option model
/// * `b=r-rf` - gives the Garman and Kohlhagen (1983) currency option model
///
/// # Return Value
/// * `f64` - the option price
/// # Examples
/// ```
/// use dprustlib::quantopts::{tw_option,OptionType};
/// println!("Call(s=100.0,x=100.0,time=1.0,tau=0.5,r=0.01,b=0.0,sigma=0.3) = {}",
///     tw_option(OptionType::Call, 100.0, 100.0, 1.0, 0.5, 0.01, 0.0, 0.3));
/// ```
pub fn tw_option(
    option_type: OptionType,
    s: f64,    // futures
    x: f64,    // strike
    time: f64, // time to end of avg period=maturity
    tau: f64,  // time to begin of avg period
    r: f64,    // interest rate
    b: f64,    // cost of carry of underlying asset
    sigma: f64,
) -> f64 {
    let m1: f64;
    let m2: f64;
    if f64::abs(b) < 1e-10f64 {
        m1 = 1.0f64;
        m2 = (2.0f64 * f64::exp(sigma * sigma * time)
            - 2.0f64 * f64::exp(sigma * sigma * tau) * (1.0f64 + sigma * sigma * (time - tau)))
            / (f64::powf(sigma, 4.0f64) * (time - tau) * (time - tau));
    } else {
        m1 = (f64::exp(b * time) - f64::exp(b * tau)) / (b * (time - tau));
        m2 = 2.0f64 * f64::exp((2.0f64 * b + sigma * sigma) * time)
            / ((b + sigma * sigma) * (2.0f64 * b + sigma * sigma) * (time - tau) * (time - tau))
            + (2.0f64 * f64::exp(tau * (2.0f64 * b + sigma * sigma)))
                / (b * (time - tau) * (time - tau))
                * (1.0f64 / (2.0f64 * b + sigma * sigma)
                    - f64::exp(b * (time - tau)) / (b + sigma * sigma));
    }
    let b_a = m1.ln() / time;
    let sigma_a = (m2.ln() / time - 2.0 * b_a).sqrt();
    gbsm_option(option_type, s, x, time, r, b_a, sigma_a)
}

pub fn tw_gbsm_pars(
    time: f64, // time to end of avg period=maturity
    tau: f64,  // time to begin of avg period
    b: f64,    // cost of carry of underlying asset
    sigma: f64,
) -> (f64, f64) {
    let m1: f64;
    let m2: f64;
    if f64::abs(b) < 1e-10f64 {
        m1 = 1.0f64;
        m2 = (2.0f64 * f64::exp(sigma * sigma * time)
            - 2.0f64 * f64::exp(sigma * sigma * tau) * (1.0f64 + sigma * sigma * (time - tau)))
            / (f64::powf(sigma, 4.0f64) * (time - tau) * (time - tau));
    } else {
        m1 = (f64::exp(b * time) - f64::exp(b * tau)) / (b * (time - tau));
        m2 = 2.0f64 * f64::exp((2.0f64 * b + sigma * sigma) * time)
            / ((b + sigma * sigma) * (2.0f64 * b + sigma * sigma) * (time - tau) * (time - tau))
            + (2.0f64 * f64::exp(tau * (2.0f64 * b + sigma * sigma)))
                / (b * (time - tau) * (time - tau))
                * (1.0f64 / (2.0f64 * b + sigma * sigma)
                    - f64::exp(b * (time - tau)) / (b + sigma * sigma));
    }
    let b_a = m1.ln() / time;
    let sigma_a = (m2.ln() / time - 2.0 * b_a).sqrt();
    (b_a, sigma_a)
}

pub fn tw_ivol(
    premium: f64,
    option_type: OptionType,
    s: f64,
    x: f64,
    time: f64,
    tau: f64,
    r: f64,
    b: f64,
    acceptable_err: f64,
    max_iterations: u32,
) -> Result<f64, f64> {
    root_brent(
        0.01,
        10.0,
        &|sigma: f64| tw_option(option_type, s, x, time, tau, r, b, sigma) - premium,
        acceptable_err,
        max_iterations,
    )
}

#[cfg(test)]
mod tests {
    use crate::quantopts::*;
    #[test]
    fn check_gbsm_option() {
        assert!(
            (gbsm_option(OptionType::Call, 100.0, 100.0, 1.0, 0.01, 0.0, 0.3)
                - 11.80489728393353f64)
                .abs()
                < 1e-12
        );
    }
    #[test]
    fn check_gbsm_delta() {
        let jnk = (10000.0 * gbsm_delta(OptionType::Call, 105.0, 100.0, 0.5, 0.10, 0.0, 0.36))
            .round()
            / 10000.0;
        assert_eq!(jnk, 0.5946f64);
        let jnk = (10000.0 * gbsm_delta(OptionType::Put, 105.0, 100.0, 0.5, 0.10, 0.0, 0.36))
            .round()
            / 10000.0;
        assert_eq!(jnk, -0.3566f64);
    }
    #[test]
    fn check_gbsm_gamma() {
        let jnk = (10000.0 * gbsm_gamma(OptionType::Call, 55.0, 60.0, 0.75, 0.1, 0.1, 0.3)).round()
            / 10000.0;
        assert_eq!(jnk, 0.0278f64);
    }
    #[test]
    fn check_gbsm_vega() {
        let jnk = (10000.0 * gbsm_vega(OptionType::Call, 55.0, 60.0, 0.75, 0.105, 0.0695, 0.3))
            .round()
            / 10000.0;
        assert_eq!(jnk, 18.5027f64);
    }
    #[test]
    fn check_gbsm_call_from_put() {
        let call_value = gbsm_option(OptionType::Call, 55.0, 60.0, 0.75, 0.105, 0.0695, 0.3);
        let put_value = gbsm_option(OptionType::Put, 55.0, 60.0, 0.75, 0.105, 0.0695, 0.3);
        assert_abs_diff_eq!(
            call_value,
            gbsm_call_from_put(put_value, 55.0, 60.0, 0.75, 0.105, 0.0695),
            epsilon = f64::powi(10.0, -10)
        );
    }
    #[test]
    fn check_gbsm_put_from_call() {
        let call_value = gbsm_option(OptionType::Call, 55.0, 60.0, 0.75, 0.105, 0.0695, 0.3);
        let put_value = gbsm_option(OptionType::Put, 55.0, 60.0, 0.75, 0.105, 0.0695, 0.3);
        assert_abs_diff_eq!(
            put_value,
            gbsm_put_from_call(call_value, 55.0, 60.0, 0.75, 0.105, 0.0695),
            epsilon = f64::powi(10.0, -10)
        );
    }
    #[test]
    fn check_gbsm_cw_approx() {
        let call_value = gbsm_option(OptionType::Call, 55.0, 60.0, 0.75, 0.105, 0.0695, 0.3);
        let approx_vol = gbsm_cw_approx_vol(
            call_value,
            OptionType::Call,
            55.0,
            60.0,
            0.75,
            0.105,
            0.0695,
        );
        assert_abs_diff_eq!(0.3, approx_vol, epsilon = 0.01);
    }
    #[test]
    fn check_gbsm_ivol() {
        let tol_digits = 8;
        let actual = 0.314159265f64;
        let tprem = gbsm_option(OptionType::Call, 100.0, 100.0, 1.0, 0.0, 0.0, actual);
        let result = gbsm_ivol(
            tprem,
            OptionType::Call,
            100.0,
            100.0,
            1.0,
            0.0,
            0.0,
            f64::powi(10.0f64, -tol_digits),
            1000000,
        )
        .unwrap();
        let difference = (result - actual).abs();
        assert!(
            difference < f64::powi(10.0f64, -tol_digits),
            "result = {}, actual = {}",
            result,
            actual
        );
    }
    #[test]
    fn check_tw_option() {
        let result = tw_option(OptionType::Call, 100.0, 100.0, 1.0, 0.5, 0.0, 0.0, 0.3);
        let actual = 9.75222938612811418579440214671194553375244140625f64;
        let difference = (result - actual).abs();
        assert!(
            difference < f64::powi(10.0f64, -24),
            "result = {}, actual = {}",
            result,
            actual
        );
    }
    #[test]
    fn check_tw_ivol() {
        let tol_digits = 8;
        let actual = 0.314159265f64;
        let tprem = tw_option(
            OptionType::Call,
            100.0,
            100.0,
            1.0,
            11.0 / 12.0,
            0.0,
            0.0,
            actual,
        );
        dbg!(tprem);
        let result = tw_ivol(
            tprem,
            OptionType::Call,
            100.0,
            100.0,
            1.0,
            11.0 / 12.0,
            0.0,
            0.0,
            f64::powi(10.0f64, -tol_digits),
            1000000000,
        )
        .unwrap();
        dbg!(result);
        let difference = (result - actual).abs();
        assert!(
            difference < f64::powi(10.0f64, -tol_digits),
            "result = {}, actual = {}",
            result,
            actual
        );
    }
}
