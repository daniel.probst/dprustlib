use chrono::{Datelike, Duration, NaiveDate, Weekday};
use std::fmt;
use std::fmt::Display;

/// Standard financial futures abbreviations for months
pub const FUTURESCODES: [char; 12] = ['F', 'G', 'H', 'J', 'K', 'M', 'N', 'Q', 'U', 'V', 'X', 'Z'];

/// Year/month data structure
#[derive(Copy, Clone, Debug, std::cmp::PartialEq)]
pub struct YearMonth {
    // the year as a signed int
    year: i32,
    // the month indexed fgrom 1 to 12
    month: u8,
}

impl Display for YearMonth {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}M{:02}", self.year, self.month)
    }
}

impl YearMonth {
    pub fn first_day(&self) -> NaiveDate {
        NaiveDate::from_ymd(self.year, self.month as u32, 1)
    }
    pub fn from<T: Datelike>(x: T) -> YearMonth {
        YearMonth::new(x.year(), x.month() as u8)
    }
    pub fn futures_fmt(&self) -> String {
        let x = format!("{}{}", FUTURESCODES[self.month as usize], self.year);
        return x;
    }
    pub fn last_day(&self) -> NaiveDate {
        let mut x = self.clone();
        x.offset(1);
        NaiveDate::from_ymd(x.year, x.month as u32, 1) - Duration::days(1)
    }
    pub fn new(year: i32, month: u8) -> YearMonth {
        if (month < 1) || (month > 12) {
            panic!("A month must be between 1..12");
        };
        YearMonth { year, month }
    }
    pub fn offset(&mut self, delta: i32) {
        let months = self.month as i32 + delta;
        self.year += (months - 1).div_euclid(12);
        self.month = ((months - 1).rem_euclid(12) + 1) as u8;
    }
}

/// Number of weekdays between two dates (including both)
///
/// Calculates the numer of Monday through Friday working days between and
/// including two dates
///
/// # Arguments
/// * `d0` - the starting date
/// * `d1` - the end date
///
/// # Return Value
/// * `u32` the number of working days
///
/// # Examples
///```
/// use dprustlib::tenors::weekday_sum;
/// use chrono::NaiveDate;
/// println!("Feb 2021 has {} weekdays.", weekday_sum(NaiveDate::from_ymd(2021,2, 1), NaiveDate::from_ymd(2021, 2, 28)));
///```
///
pub fn weekday_sum(d0: NaiveDate, d1: NaiveDate) -> u32 {
    let ndays = (d1 - d0).num_days() + 1;
    (0..ndays)
        .map(|i| match (d0 + Duration::days(i)).weekday() {
            Weekday::Sat => 0,
            Weekday::Sun => 0,
            _ => 1,
        })
        .sum()
}

// https://stackoverflow.com/questions/31210357/is-there-a-modulus-not-remainder-function-operation
pub fn n_mod_m<T: std::ops::Rem<Output = T> + std::ops::Add<Output = T> + Copy>(n: T, m: T) -> T {
    ((n % m) + m) % m
}

#[cfg(test)]
mod tests {
    use crate::tenors::*;
    #[test]
    fn check_offset_yearmonth1() {
        let my_tenor = YearMonth::new(2020, 6);
        let mut my_tenor_test = YearMonth {
            year: 2019,
            month: 11,
        };
        my_tenor_test.offset(7);
        assert_eq!(my_tenor_test, my_tenor);
    }
    #[test]
    fn check_offset_yearmonth2() {
        let mut a = YearMonth::new(2021, 12);
        a.offset(1);
        let b = YearMonth::new(2022, 1);
        assert_eq!(a, b);
    }
    #[test]
    fn check_offset_yearmonth3() {
        let mut a = YearMonth::new(2021, 12);
        a.offset(13);
        let b = YearMonth::new(2023, 1);
        assert_eq!(a, b);
    }
    #[test]
    fn check_offset_yearmonth4() {
        let mut a = YearMonth::new(2021, 1);
        a.offset(-1);
        let b = YearMonth::new(2020, 12);
        assert_eq!(a, b);
    }
    #[test]
    fn check_offset_yearmonth5() {
        let mut a = YearMonth::new(2021, 11);
        a.offset(1);
        let b = YearMonth::new(2021, 12);
        assert_eq!(a, b);
    }
}
