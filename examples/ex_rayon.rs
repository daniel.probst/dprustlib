use dprustlib::mathutils::SampleStats;
use dprustlib::mcopts::{asian_mc, AsianGBM};
use dprustlib::quantopts::{GBSMData, OptionType};

fn main() {
    let f: f64 = 100.0;
    let k: f64 = 100.0;
    let m: usize = 10;
    let n: usize = 10;
    let dt: f64 = 1.0f64 / 365f64;
    let r: f64 = 0.01f64;
    let b: f64 = 0.0f64;
    let sigma: f64 = 0.3f64;

    asian_mn_sens(f, k, m, n, dt, r, b, sigma, 100000000, 1e-3);
}

fn asian_mn_sens(
    f: f64,
    k: f64,
    m: usize,
    n: usize,
    dt: f64,
    r: f64,
    b: f64,
    sigma: f64,
    max_iter: usize,
    max_err: f64,
) {
    println!("Underlying: {}", f);
    println!("Strike: {}", k);
    println!("m: {}", m);
    println!("n: {}", n);
    println!("dt (1/{}): {}", 1.0 / dt, dt);
    println!("r: {}", r);
    println!("b: {}", b);
    println!("sigma: {}", sigma);
    let res = asian_mn_run(f, k, m, n, dt, r, b, sigma, max_iter, max_err);
    let eres = res.eval();
    println!("{} {} {}", res.n, res.sum, res.ss);
    println!("{} {} {}", eres.0, eres.1, eres.2);
}

fn asian_mn_run(
    f: f64,
    k: f64,
    m: usize,
    n: usize,
    dt: f64,
    r: f64,
    b: f64,
    sigma: f64,
    max_iter: usize,
    max_err: f64,
) -> SampleStats {
    let settle_vec: Vec<bool> = vec![true; n];
    let mut mu_vec: Vec<f64> = vec![0.0f64; n];
    let mut sd_vec: Vec<f64> = vec![0.0f64; n];
    // setup mu and sd
    let mu = (b - 0.5f64 * sigma * sigma) * ((1.0f64 + m as f64) * dt);
    mu_vec[0] = mu;
    let sd = sigma * ((1.0f64 + m as f64) * dt).sqrt();
    sd_vec[0] = sd;
    let mu = (b - 0.5f64 * sigma * sigma) * dt;
    let sd = sigma * dt.sqrt();
    for i in 1..n {
        mu_vec[i] = mu;
        sd_vec[i] = sd;
    }
    // prep the structs
    let gbm = AsianGBM {
        settle: settle_vec,
        mu: mu_vec,
        sd: sd_vec,
        x: f,
        pre: None,
    };
    let gbsm_data = GBSMData {
        option_type: OptionType::Call,
        asset_price: f,
        strike: k,
        r: r,
        b: b,
        time: dt * (n + m) as f64,
        sigma: sigma,
    };
    // run the asian
    asian_mc(gbm, gbsm_data, max_iter, max_err)
}
