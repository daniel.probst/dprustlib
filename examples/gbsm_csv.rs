use dprustlib::quantopts::{GBSMData, OptionType};
use std::io;

fn main() {
    let optdat: GBSMData = GBSMData {
        option_type: OptionType::Call,
        asset_price: 100.0,
        strike: 100.0,
        r: 0.01,
        b: 0.0,
        time: 1.0,
        sigma: 0.3,
    };
    println!("{}", optdat);
    println!("Premium: {}", optdat.gbsm_option());
    println!("Starting read from stdin");
    let mut rdr = csv::Reader::from_reader(io::stdin());
    // Loop over each record.
    for result in rdr.deserialize() {
        let record: GBSMData = result.unwrap();
        println!("{}, Premium: {}", record, record.gbsm_option());
    }
}
