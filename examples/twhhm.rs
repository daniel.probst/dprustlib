use dprustlib::quantopts::*;

fn main() {
    println!("Hello Dan.");
    println!(
        "gbsm_option(OptionType::Call, 100.0, 100.0, 1.0, 0.01, 0.0, 0.3):{}",
        gbsm_option(OptionType::Call, 100.0, 100.0, 1.0, 0.01, 0.0, 0.3)
    );
    println!(
        "gbsm_option(OptionType::Put, 75.0, 70.0, 0.5, 0.1, 0.05, 0.35):{}",
        gbsm_option(OptionType::Put, 75.0, 70.0, 0.5, 0.1, 0.05, 0.35)
    );
    println!(
        "gbsm_option(OptionType::Put, 19.0, 19.0, 0.75, 0.1, 0.0, 0.28):{}",
        gbsm_option(OptionType::Put, 19.0, 19.0, 0.75, 0.1, 0.0, 0.28)
    );
    println!(
        "gbsm_option(OptionType::Put, exp(-r*t)a*19.0, 19.0, 0.75, 0.1, 0.0, 0.28):{}",
        gbsm_option(
            OptionType::Put,
            (-0.1f64 * 0.75).exp() * 19.0,
            19.0,
            0.75,
            0.1,
            0.0,
            0.28
        )
    );
    println!(
        "tw_option(OptionType::Put, 100.0,100.0,1.0,11.0/12.0,0.01,0.0,0.3):{}",
        tw_option(
            OptionType::Put,
            100.0,
            100.0,
            1.0,
            11.0 / 12.0,
            0.01,
            0.0,
            0.3
        )
    );
    println!(
        "tw_option(OptionType::Put, 100.0,100.0,1.0,11.0/12.0,0.01,0.0,0.3):{}",
        hhm_option(
            OptionType::Call,
            100.0,
            110.0,
            105.0,
            0.0,
            0.5,
            360,
            180,
            0.07,
            0.02,
            0.25
        )
    );
    println!("\n HHM Test - Table 4-26 p.194");
    hhmtest(0.0, 0.5, 27, 0, 0.1);
    hhmtest(0.0, 0.5, 27, 0, 0.2);
    hhmtest(0.0, 0.5, 27, 0, 0.3);
    hhmtest(10.0 / 52.0, 0.5 + 10.0 / 52.0, 27, 0, 0.1);
    hhmtest(10.0 / 52.0, 0.5 + 10.0 / 52.0, 27, 0, 0.2);
    hhmtest(10.0 / 52.0, 0.5 + 10.0 / 52.0, 27, 0, 0.3);
    hhmtest(20.0 / 52.0, 0.5 + 20.0 / 52.0, 27, 0, 0.1);
    hhmtest(20.0 / 52.0, 0.5 + 20.0 / 52.0, 27, 0, 0.2);
    hhmtest(20.0 / 52.0, 0.5 + 20.0 / 52.0, 27, 0, 0.3);
}

fn hhmtest(t1: f64, t: f64, n: u32, m: i32, sigma: f64) {
    println!(
        "hhm_option(Call,s=100,sa=-100.0;x=100.0,t1={},t={},n={},m={},r=0.08,b=0.03,sigma={}): {}",
        t1,
        t,
        n,
        m,
        sigma,
        hhm_option(
            OptionType::Call,
            100.0,
            -100.0,
            100.0,
            t1,
            t,
            n,
            m,
            0.08,
            0.03,
            sigma
        )
    )
}
