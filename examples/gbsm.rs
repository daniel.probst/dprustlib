//! command line program for generalized Black Scholes Merton

#![deny(missing_docs)]
use clap::{ArgEnum, Args, Parser, Subcommand};
use dprustlib::quantopts::*;

#[derive(Parser, Debug)]
#[clap(author, version, about = "GBSM option pricing cli app", long_about = None)]
struct Cli {
    /// Type of option
    #[clap(arg_enum, default_value_t = ArgOptionType::Call)]
    arg_option_type: ArgOptionType,
    /// Underlying asset/forward price
    #[clap(short, long, default_value = "100.0")]
    forward: f64,
    /// Option strike price
    #[clap(short = 'k', long)]
    strike: Option<f64>,
    /// Interest rate
    #[clap(short, default_value = "0.01")]
    r: f64,
    /// Black76:0, BlackScholes73:r, BSM73:r-q
    #[clap(short, default_value = "0.0")]
    b: f64,
    /// Time in years
    #[clap(short, long, default_value = "1.0")]
    time: f64,
    /// Volatility per year
    #[clap(short = 's', long, default_value = "0.3")]
    sigma: f64,
    /// verbosity level
    #[clap(short, long, parse(from_occurrences))]
    verbosity: usize,
    #[clap(subcommand)]
    command: Option<Commands>,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
enum ArgOptionType {
    Call,
    Put,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Implied volatility calculation
    Ivol(CliIvol),
}

#[derive(Args, Debug)]
struct CliIvol {
    /// Maximum error of root search
    #[clap(long, default_value = "1e-7")]
    maxerr: f64,
    /// Maximum number of root iterations
    #[clap(long, default_value = "1000000000")]
    maxiter: u32,
    /// Premium of option
    #[clap(short, long)]
    premium: f64,
}

fn main() {
    let cli = Cli::parse();
    if cli.verbosity > 1 {
        println!("Value for cli: {:#?}", cli);
    }
    let option_type: OptionType = match cli.arg_option_type {
        ArgOptionType::Call => OptionType::Call,
        ArgOptionType::Put => OptionType::Put,
    };
    let k: f64 = match cli.strike {
        Some(x) => x,
        None => cli.forward,
    };
    let sigma: f64;
    match &cli.command {
        Some(Commands::Ivol(cli_ivol)) => {
            sigma = gbsm_ivol(
                cli_ivol.premium,
                option_type,
                cli.forward,
                k,
                cli.time,
                cli.r,
                cli.b,
                cli_ivol.maxerr,
                cli_ivol.maxiter,
            )
            .unwrap()
        }
        None => sigma = cli.sigma,
    }
    let gbsm_data: GBSMData = GBSMData {
        option_type,
        asset_price: cli.forward,
        strike: k,
        time: cli.time,
        r: cli.r,
        b: cli.b,
        sigma,
    };
    println!("{}", gbsm_data);
    println!("Premium: {}", gbsm_data.gbsm_option());
    println!("Delta: {}", gbsm_data.gbsm_delta());
    println!("Gamma: {}", gbsm_data.gbsm_gamma());
    let theta = gbsm_data.gbsm_theta();
    println!("Theta: {} [/365:{}]", theta, theta / 365.0);
    let vega = gbsm_data.gbsm_vega();
    println!("Vega: {} [/100:{}]", vega, vega / 100.0);
}
