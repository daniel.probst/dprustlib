use dprustlib::{
    mathutils::{self, SampleStats},
    quantopts::*,
};
use rayon::prelude::*;

fn main() {
    let mydata: GBSMData = GBSMData {
        option_type: OptionType::Call,
        asset_price: 100.0,
        strike: 100.0,
        r: 0.01,
        b: 0.0,
        time: 1.0,
        sigma: 0.3,
    };
    //let mut ss: SampleStats = mathutils::SampleStats::new();
    //for i in 1..100000000 {
    //let tmp = mydata.random_sample();
    //ss.update(tmp);
    //if i % 100000 == 0 {
    //println!("sample:{}", tmp);
    //println!("sample stats:{}", ss);
    //let (a, b, c) = ss.eval();
    //println!("sample stats: ({},{},{})", a, b, c,);
    //}
    //}
    println!("{}", mydata);
    println!("Premium:{}", mydata.gbsm_option());
    let ss = gbsm_mc(mydata, 10000000000, 0.0001);
    let (a, b, c) = ss.eval();
    println!("sample stats: ({},{},{})", a, b, c,);
}

fn gbsm_mc(gbsm_data: GBSMData, max_iter: usize, max_err: f64) -> SampleStats {
    // Run a first block until an option sample value is non-zero
    let mut sample_stats = mathutils::SampleStats::new();
    while sample_stats.n <= max_iter {
        sample_stats.update(gbsm_data.random_sample());
        if sample_stats.sum != 0.0 && sample_stats.n > 1 {
            break;
        }
    }
    // now we can iterate until precision is ok
    while sample_stats.eval().2 > max_err {
        //iter version
        let items = (0..1000000).into_par_iter();
        let res = items
            .map(|_x| gbsm_data.random_sample())
            .map(|x| SampleStats::init(x))
            .reduce(
                || SampleStats::new(),
                |sum, item| SampleStats::merge(&sum, &item),
            );
        sample_stats.add(&res);
        //dbg!(sample_stats.eval());
        if sample_stats.n > max_iter {
            println!("Breaking on max_iter. Todo doc better.");
            break;
        }
    }
    sample_stats
}
