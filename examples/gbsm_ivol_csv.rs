use dprustlib::quantopts::GBSMMarketData;
use std::io;

fn main() {
    println!("Starting read from stdin");
    let mut rdr = csv::Reader::from_reader(io::stdin());
    // Loop over each record.
    for result in rdr.deserialize() {
        let record: GBSMMarketData = result.unwrap();
        println!(
            "{}, implied volatility: {:?}",
            record,
            record.gbsm_ivol(0.01, 1000)
        );
    }
}
