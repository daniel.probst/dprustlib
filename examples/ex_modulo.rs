fn n_mod_m<T: std::ops::Rem<Output = T> + std::ops::Add<Output = T> + Copy>(n: T, m: T) -> T {
    ((n % m) + m) % m
}

fn main() {
    println!("mod( 13-1,12)+1 {}", n_mod_m(12, 12) + 1);
    println!("mod( 15-1,12)+1 {}", n_mod_m(14, 12) + 1);
    println!("mod( 1-1,12)+1 {}", n_mod_m(0, 12) + 1);
    println!("mod( 0-1,12)+1 {}", n_mod_m(-1, 12) + 1);
    println!("mod( -1-1,12)+1 {}", n_mod_m(-2, 12) + 1);
}
