use chrono::{Datelike, Duration, NaiveDate, Weekday};
use dprustlib::quantopts::*;
use dprustlib::tenors::*;
use std::convert::TryInto;

fn main() {
    println!("Hello Dan.");
    let tday = chrono::offset::Utc::today().naive_utc();
    let cob = tday - Duration::days(1);
    println!("Today is {}", tday);
    println!("Starting Analysis COB {}", cob);
    println!("\n");
    testfn(cob, 0);
    println!("\n");
    testfn(cob, 1);
}

fn testfn(cob: NaiveDate, o: i32) {
    let s: f64 = 70.0f64;
    let x: f64 = 70.0f64;
    let r = 0.01f64;
    let b = 0.0f64;
    let sigma = 0.3;
    println!("COB:{}, offset:{}", cob, o);
    let mut ym = YearMonth::from(cob);
    ym.offset(o);
    println!("ym:{}", ym);
    let d2 = ym.last_day();
    ym.offset(-1);
    let mut d1 = ym.last_day();
    if o == 0 && (d1 - cob).num_days() < 0 {
        d1 = cob;
    }
    println!("COB:{},d1:{},d2:{}", cob, d1, d2);
    println!("Calendar day version.");
    let pre_days = (d1 - cob).num_days();
    let total_days = (d2 - cob).num_days();
    let avg_days = total_days - pre_days;
    println!(
        "The tau range {} {} contains {} calendar days.",
        cob, d1, pre_days
    );
    println!(
        "The T range {} {} contains {} calendar days.",
        cob, d2, total_days
    );
    println!("Averaging over {} calendar days.", avg_days);
    let tau: f64 = pre_days as f64 / 365.0f64;
    let t: f64 = total_days as f64 / 365.0f64;
    println!(
        "tw_option(Call,{},{},{},{},{},{},{}):{}",
        s,
        x,
        t,
        tau,
        r,
        b,
        sigma,
        tw_option(OptionType::Call, s, x, t, tau, r, b, sigma)
    );
    println!(
        "hhm_option(Call,{},-1.0,{},{},{},{},0,0.01,0.0,{}):{}",
        s,
        x,
        tau,
        t,
        avg_days,
        sigma,
        hhm_option(
            OptionType::Call,
            s,
            -100.0,
            x,
            tau,
            t,
            avg_days.try_into().unwrap(),
            0,
            0.01,
            0.0,
            sigma
        )
    );
    println!("Working day version.");
    let pre_days = weekday_sum(cob, d1);
    let total_days = weekday_sum(cob, d2);
    let avg_days = total_days - pre_days;
    println!(
        "The tau range {} {} contains {} working days.",
        cob, d1, pre_days
    );
    println!(
        "The T range {} {} contains {} working days.",
        cob, d2, total_days
    );
    println!("Averaging over {} working days.", avg_days);
    let tau: f64 = pre_days as f64 / 260.0f64;
    let t: f64 = total_days as f64 / 260.0f64;
    println!(
        "tw_option(Call,{},{},{},{},{},{},{}):{}",
        s,
        x,
        t,
        tau,
        r,
        b,
        sigma,
        tw_option(OptionType::Call, s, x, t, tau, r, b, sigma)
    );
    println!(
        "hhm_option(Call,{},-1.0,{},{},{},{},0,0.01,0.0,{}):{}",
        s,
        x,
        tau,
        t,
        avg_days,
        sigma,
        hhm_option(
            OptionType::Call,
            s,
            -100.0,
            x,
            tau,
            t,
            avg_days.try_into().unwrap(),
            0,
            0.01,
            0.0,
            sigma
        )
    );
}

pub fn weekday_sum(d0: NaiveDate, d1: NaiveDate) -> u32 {
    let ndays = (d1 - d0).num_days() + 1;
    (0..ndays)
        .map(|i| match (d0 + Duration::days(i)).weekday() {
            Weekday::Sat => 0,
            Weekday::Sun => 0,
            _ => 1,
        })
        .sum()
}
