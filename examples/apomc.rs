//! command line program for monte carlo based pricing of average price options

#![deny(missing_docs)]
use chrono::{Datelike, Duration, NaiveDate, Weekday};
use clap::{ArgEnum, Args, Parser, Subcommand};
use dprustlib::mathutils::SampleStats;
use dprustlib::mcopts::{asian_mc, AsianGBM};
use dprustlib::quantopts::{GBSMData, OptionType};
use dprustlib::tenors::*;
use env_logger::Builder;
use log::{debug, info, LevelFilter};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// Log level verbosity
    #[clap(short, long, parse(from_occurrences))]
    verbosity: usize,
    /// Maximum std error of mean of monte carlo
    #[clap(long, default_value = "1e-3")]
    maxerr: f64,
    /// Maximum number of monte carlo runs
    #[clap(long, default_value = "1000000000")]
    maxiter: usize,
    /// Type of option
    #[clap(arg_enum, default_value_t = ArgOptionType::Call)]
    arg_option_type: ArgOptionType,
    /// Underlying asset/forward price
    #[clap(short, long, default_value = "100.0")]
    forward: f64,
    /// Option strike price
    #[clap(short = 'k', long)]
    strike: Option<f64>,
    /// Interest rate
    #[clap(short, default_value = "0.01")]
    r: f64,
    /// Black76:0, BlackScholes73:r, BSM73:r-q
    #[clap(short, default_value = "0.0")]
    b: f64,
    /// Volatility per year
    #[clap(short = 's', long, default_value = "0.3")]
    sigma: f64,
    /// Number/weight of previous settlement average (optional, requires xm)
    #[clap(long, requires = "xm")]
    w: Option<usize>,
    /// Average of previous settlements (optional, requires w)
    #[clap(long, requires = "w")]
    xm: Option<f64>,
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
enum ArgOptionType {
    Call,
    Put,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Calender month mode
    Cal(CliCal),
    ///  "m & n" mode with m non-pricing and n pricing steps
    Mn(CliMn),
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
enum ArgSettleType {
    CalDays,
    WorkDays,
}

#[derive(Args, Debug)]
struct CliCal {
    /// Date of last COB (last day settled) [default: yesterday]
    #[clap(short, long, default_value = "yesterday",parse(try_from_str=cob_parse))]
    date: NaiveDate,
    /// 2nd underlying forward price for last period of calmonth (optional)
    #[clap(long)]
    forward2: Option<f64>,
    /// 2nd volatility for last period of calmonth (optional)
    #[clap(long)]
    sigma2: Option<f64>,
    /// Offset in # of months (0:balmo, 1:M1, 2:M2,...)
    #[clap(short, long, default_value = "0")]
    offset: usize,
    /// Filter settlement on WorkDays or CalDays
    #[clap(arg_enum, default_value_t = ArgSettleType::WorkDays)]
    arg_settle_type: ArgSettleType,
}

#[derive(Args, Debug)]
struct CliMn {
    /// Number of pre-settlement time steps
    #[clap(short, long, default_value = "0")]
    m: usize,
    /// Number of settlement time steps
    #[clap(short, long, default_value = "22")]
    n: usize,
    /// Number of time steps (days) per base period (year)
    #[clap(long, default_value = "260")]
    ndays: usize,
}

fn cob_parse(s: &str) -> Result<NaiveDate, String> {
    match s {
        "yesterday" => Ok(chrono::offset::Utc::today().naive_utc() - Duration::days(1)),
        //_ => Err(format!("Cannot parse {} as date", s)),
        _ => Ok(s.parse::<NaiveDate>().expect("Unable to parse date.")),
    }
}
//4     } else {
//6     }

fn main() {
    let cli = Cli::parse();

    // Set logging level
    // match option - default level is Error
    let log_level = match cli.verbosity {
        0 => LevelFilter::Warn, //make at least warn default
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    // set it
    Builder::new().filter_level(log_level).init();
    debug!("Value for cli: {:#?}", cli);
    // Either time-step or calmonth
    match &cli.command {
        Commands::Cal(x) => asian_calmonth(&cli, &x),
        Commands::Mn(x) => asian_mn(&cli, &x),
    }
}

fn asian_calmonth(cli: &Cli, cli_cal: &CliCal) {
    info!("Going into calendar month mode");
    // option type
    let option_type: OptionType = match cli.arg_option_type {
        ArgOptionType::Call => OptionType::Call,
        ArgOptionType::Put => OptionType::Put,
    };
    // underlying
    let f: f64 = cli.forward;
    let f2: f64 = match cli_cal.forward2 {
        Some(x) => x,
        None => f,
    };
    // if strike is not specified then make it ATM
    // refac this to weighted avg?
    // TODO refac to vector of inputs
    let k: f64 = match cli.strike {
        Some(x) => x,
        None => f,
    };
    // The complicated date stuff
    // define COB
    let cob: NaiveDate = cli_cal.date;
    // define ym
    let offset: usize = cli_cal.offset;
    let mut ym = YearMonth::from(cob);
    ym.offset(offset as i32);
    info!("cob: {}, ym:{}", cob, ym);
    let d2 = ym.last_day();
    if d2 == cob {
        panic!("Close of business is last day of settlement month: nothing to do!");
    }
    let mut d1 = ym.first_day();
    d1 = std::cmp::max(cob + Duration::days(1), d1);
    debug!("COB:{},d1:{},d2:{}", cob, d1, d2);
    let nobs: usize = ((d2 - d1).num_days() + 1) as usize;
    let npre: usize = (d1 - cob).num_days() as usize;
    debug!("nobs: {}, npre:{}", nobs, npre);
    // Vector of all month dates
    let day_vec = d1
        .iter_days()
        .take(nobs as usize)
        .collect::<Vec<NaiveDate>>();
    debug!("Target month dates: {:?}", day_vec);
    let wday_vec = day_vec
        .iter()
        .map(|x| match x.weekday() {
            Weekday::Sat => 0,
            Weekday::Sun => 0,
            _ => 1,
        })
        .collect::<Vec<usize>>();
    let settle_vec = wday_vec
        .iter()
        .map(|x| match x {
            0 => false,
            _ => true,
        })
        .collect::<Vec<bool>>();
    debug!("Target month bizday vector: {:?}", wday_vec);
    // xm, w
    let pre: Option<(f64, usize)> = match (cli.xm, cli.w) {
        (Some(x), Some(y)) => Some((x, y)),
        _ => None,
    };
    // r b sigma
    let r: f64 = cli.r;
    let b: f64 = cli.b;
    let sigma: f64 = cli.sigma;
    let sigma2: f64 = match cli_cal.sigma2 {
        Some(x) => x,
        None => sigma,
    };

    // max_iter, max_err
    let max_iter: usize = cli.maxiter;
    let max_err: f64 = cli.maxerr;

    let n = nobs;
    let m = npre - 1;

    info!(
        "asien_calmonth_run(type:{},f:{},f2:{},k:{},pre:{:?},settle_vec:{:?},m:{},n:{},r:{},b:{},s1:{},s2:{},max_iter:{},max_err:{})",
        option_type,f, f2, k, pre,settle_vec,m,n,r,b,sigma,sigma2,max_iter,max_err,
    );
    let res_std = asian_calmonth_run(
        option_type,
        f,
        f2,
        k,
        pre,
        &settle_vec,
        m,
        n,
        r,
        b,
        sigma,
        sigma2,
        max_iter,
        max_err,
    );
    debug!("Standard premium took {} steps to converge.", res_std.n);
    let eres_std = res_std.eval();
    println!("premium: {} +-[{}]", eres_std.0, eres_std.2);

    // check delta
    let res_delta = asian_calmonth_run(
        option_type,
        1.01 * f,
        1.01 * f2,
        k,
        pre,
        &settle_vec,
        m,
        n,
        r,
        b,
        sigma,
        sigma2,
        max_iter,
        max_err,
    );
    debug!("Delta took {} steps to converge.", res_delta.n);
    let eres_delta = res_delta.eval();
    println!(
        "delta {}->{}: {}",
        f,
        1.01 * f,
        (eres_delta.0 - eres_std.0) / (0.01 * f)
    );
    //  strikedelta
    let res_strikedelta = asian_calmonth_run(
        option_type,
        f,
        f2,
        1.01 * k,
        pre,
        &settle_vec,
        m,
        n,
        r,
        b,
        sigma,
        sigma2,
        max_iter,
        max_err,
    );
    debug!("StrikeDelta took {} steps to converge.", res_strikedelta.n);
    let eres_strikedelta = res_strikedelta.eval();
    println!(
        "strike-delta {}->{}: {}",
        k,
        1.01 * k,
        (eres_strikedelta.0 - eres_std.0) / (0.01 * k)
    );
    // vega
    let res_vega = asian_calmonth_run(
        option_type,
        f,
        f2,
        k,
        pre,
        &settle_vec,
        m,
        n,
        r,
        b,
        sigma + 0.01,
        sigma2 + 0.01,
        max_iter,
        max_err,
    );
    debug!("Vega took {} steps to converge.", res_vega.n);
    let eres_vega = res_vega.eval();
    println!(
        "vega {}->{}: {}",
        sigma,
        sigma + 0.01,
        eres_vega.0 - eres_std.0
    );
    if m > 0 {
        let res_m = asian_calmonth_run(
            option_type,
            f,
            f2,
            k,
            pre,
            &settle_vec,
            m - 1,
            n,
            r,
            b,
            sigma,
            sigma2,
            max_iter,
            max_err,
        );
        debug!("m Delta took {} steps to converge.", res_m.n);
        let eres_m = res_m.eval();
        println!("m-delta {}->{}: {}", m, m - 1, eres_m.0 - eres_std.0);
    }
}

fn asian_calmonth_run(
    option_type: OptionType,
    f: f64,
    _f2: f64,
    k: f64,
    pre: Option<(f64, usize)>,
    settle_vec: &Vec<bool>,
    m: usize,
    n: usize,
    r: f64,
    b: f64,
    sigma: f64,
    _sigma2: f64,
    max_iter: usize,
    max_err: f64,
) -> SampleStats {
    let dt: f64 = 1.0 / 365.0;
    let n_settle = settle_vec.len();
    let mut mu_vec: Vec<f64> = vec![0.0f64; n_settle];
    let mut sd_vec: Vec<f64> = vec![0.0f64; n_settle];
    // setup mu and sd
    let mu = (b - 0.5f64 * sigma * sigma) * ((1.0f64 + m as f64) * dt);
    mu_vec[0] = mu;
    let sd = sigma * ((1.0f64 + m as f64) * dt).sqrt();
    sd_vec[0] = sd;
    let mu = (b - 0.5f64 * sigma * sigma) * dt;
    let sd = sigma * dt.sqrt();
    for i in 1..n {
        mu_vec[i] = mu;
        sd_vec[i] = sd;
    }
    // prep the structs
    let gbm = AsianGBM {
        settle: settle_vec.to_vec(),
        mu: mu_vec,
        sd: sd_vec,
        x: f,
        pre,
    };
    debug!("{}", gbm);
    let gbsm_data = GBSMData {
        option_type,
        asset_price: f,
        strike: k,
        r,
        b,
        time: dt * (n + m) as f64,
        sigma,
    };
    debug!("{}", gbsm_data);
    // run the asian
    asian_mc(gbm, gbsm_data, max_iter, max_err)
}

fn asian_mn(cli: &Cli, cli_mn: &CliMn) {
    info!("Going into time step mode");
    // option type
    let option_type: OptionType = match cli.arg_option_type {
        ArgOptionType::Call => OptionType::Call,
        ArgOptionType::Put => OptionType::Put,
    };
    // underlying
    let f: f64 = cli.forward;
    // if strike is not specified then make it ATM
    // refac this to weighted avg?
    // TODO refac to vector of inputs
    let k: f64 = match cli.strike {
        Some(x) => x,
        None => f,
    };
    // m, n, dt and ndays
    let n: usize = cli_mn.n;
    let m: usize = cli_mn.m;
    let dt: f64 = 1.0 / cli_mn.ndays as f64;
    info!(
        "pre settle m: {}, settle n: {}, dt: {} (1/{})",
        m, n, dt, cli_mn.ndays
    );
    // xm, w
    let pre: Option<(f64, usize)> = match (cli.xm, cli.w) {
        (Some(x), Some(y)) => Some((x, y)),
        _ => None,
    };
    // r b sigma
    let r: f64 = cli.r;
    let b: f64 = cli.b;
    let sigma: f64 = cli.sigma;
    // max_iter, max_err
    let max_iter: usize = cli.maxiter;
    let max_err: f64 = cli.maxerr;
    // max_iter, max_err
    // run the stuff
    info!(
        "asien_mn_run(type:{},f:{},k:{},m:{},n:{},pre:{:?},dt:{},r:{},b:{},sigma:{},,max_iter:{},max_err:{})",
        option_type,f, k, m,n, pre, dt,r,b,sigma,max_iter,max_err,
    );
    let res_std = asian_mn_run(
        option_type,
        f,
        k,
        m,
        n,
        pre,
        dt,
        r,
        b,
        sigma,
        max_iter,
        max_err,
    );
    debug!("Standard premium took {} steps to converge.", res_std.n);
    let eres_std = res_std.eval();
    println!("premium: {} +-[{}]", eres_std.0, eres_std.2);
    // check delta
    let res_delta = asian_mn_run(
        option_type,
        1.01 * f,
        k,
        m,
        n,
        pre,
        dt,
        r,
        b,
        sigma,
        max_iter,
        max_err,
    );
    debug!("Delta took {} steps to converge.", res_delta.n);
    let eres_delta = res_delta.eval();
    println!(
        "delta {}->{}: {}",
        f,
        1.01 * f,
        (eres_delta.0 - eres_std.0) / (0.01 * f)
    );
    //  strikedelta
    let res_strikedelta = asian_mn_run(
        option_type,
        f,
        1.01 * k,
        m,
        n,
        pre,
        dt,
        r,
        b,
        sigma,
        max_iter,
        max_err,
    );
    debug!("StrikeDelta took {} steps to converge.", res_strikedelta.n);
    let eres_strikedelta = res_strikedelta.eval();
    println!(
        "strike-delta {}->{}: {}",
        k,
        1.01 * k,
        (eres_strikedelta.0 - eres_std.0) / (0.01 * k)
    );
    // vega
    let res_vega = asian_mn_run(
        option_type,
        f,
        k,
        m,
        n,
        pre,
        dt,
        r,
        b,
        sigma + 0.01,
        max_iter,
        max_err,
    );
    debug!("Vega took {} steps to converge.", res_vega.n);
    let eres_vega = res_vega.eval();
    println!(
        "vega {}->{}: {}",
        sigma,
        sigma + 0.01,
        eres_vega.0 - eres_std.0
    );
    // m delta
    if m > 0 {
        let res_m = asian_mn_run(
            option_type,
            f,
            k,
            m - 1,
            n,
            pre,
            dt,
            r,
            b,
            sigma,
            max_iter,
            max_err,
        );
        debug!("m Delta took {} steps to converge.", res_m.n);
        let eres_m = res_m.eval();
        println!("m-delta {}->{}: {}", m, m - 1, eres_m.0 - eres_std.0);
    }
}

fn asian_mn_run(
    option_type: OptionType,
    f: f64,
    k: f64,
    m: usize,
    n: usize,
    pre: Option<(f64, usize)>,
    dt: f64,
    r: f64,
    b: f64,
    sigma: f64,
    max_iter: usize,
    max_err: f64,
) -> SampleStats {
    let settle_vec: Vec<bool> = vec![true; n];
    let mut mu_vec: Vec<f64> = vec![0.0f64; n];
    let mut sd_vec: Vec<f64> = vec![0.0f64; n];
    // setup mu and sd
    let mu = (b - 0.5f64 * sigma * sigma) * ((1.0f64 + m as f64) * dt);
    mu_vec[0] = mu;
    let sd = sigma * ((1.0f64 + m as f64) * dt).sqrt();
    sd_vec[0] = sd;
    let mu = (b - 0.5f64 * sigma * sigma) * dt;
    let sd = sigma * dt.sqrt();
    for i in 1..n {
        mu_vec[i] = mu;
        sd_vec[i] = sd;
    }
    // prep the structs
    let gbm = AsianGBM {
        settle: settle_vec,
        mu: mu_vec,
        sd: sd_vec,
        x: f,
        pre,
    };
    let gbsm_data = GBSMData {
        option_type,
        asset_price: f,
        strike: k,
        r,
        b,
        time: dt * (n + m) as f64,
        sigma,
    };
    // run the asian
    asian_mc(gbm, gbsm_data, max_iter, max_err)
}
