use dprustlib::quantopts::{GBSMData, OptionType};

fn main() {
    let optdat: GBSMData = GBSMData {
        option_type: OptionType::Call,
        asset_price: 100.0,
        strike: 100.0,
        r: 0.01,
        b: 0.0,
        time: 1.0,
        sigma: 0.3,
    };
    println!("{}", optdat);
    println!("Premium: {}", optdat.gbsm_option());
}
