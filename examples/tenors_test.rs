use dprustlib::tenors::*;

fn main() {
    let tenor = YearMonth::new(2021, 11);
    println!("tenor: {}", tenor);
    println!("tenor.first_day(): {}", tenor.first_day());
    println!("tenor.last_day(): {}", tenor.last_day());

    mydiv(-13);
    mydiv(-12);
    mydiv(-11);
    mydiv(-1);
    mydiv(0);
    mydiv(1);
    mydiv(11);
    mydiv(12);
    mydiv(13);
    mydiv(23);
    mydiv(24);
    mydiv(25);
}

fn mydiv(x: i32) {
    println!(
        "{} {} {}",
        x,
        (x - 1).div_euclid(12),
        (x - 1).rem_euclid(12) + 1,
    );
}
