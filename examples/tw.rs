//! Turnbull Wakeman CLI

#![deny(missing_docs)]
use clap::{ArgEnum, Args, Parser, Subcommand};
use dprustlib::quantopts::*;

#[derive(Parser, Debug)]
#[clap(author, version, about = "Turnbull Wakemann option pricing cli app", long_about = None)]
struct Cli {
    /// Type of option
    #[clap(arg_enum, default_value_t = ArgOptionType::Call)]
    arg_option_type: ArgOptionType,
    /// Underlying asset/forward price
    #[clap(short, long, default_value = "100.0")]
    forward: f64,
    /// Option strike price
    #[clap(short = 'k', long)]
    strike: Option<f64>,
    /// Time in years until end of average period
    #[clap(short, long, default_value = "1.0")]
    time: f64,
    /// Time in years until start of average period
    #[clap(long, default_value = "0.0")]
    tau: f64,
    /// Interest rate
    #[clap(short, default_value = "0.01")]
    r: f64,
    /// Black76:0, BlackScholes73:r, BSM73:r-q
    #[clap(short, default_value = "0.0")]
    b: f64,
    /// Volatility per year
    #[clap(short = 's', long, default_value = "0.3")]
    sigma: f64,
    /// verbosity level
    #[clap(short, long, parse(from_occurrences))]
    verbosity: usize,
    #[clap(subcommand)]
    command: Option<Commands>,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
enum ArgOptionType {
    Call,
    Put,
}

#[derive(Debug, Subcommand)]
enum Commands {
    /// Implied volatility calculation
    Ivol(CliIvol),
}

#[derive(Args, Debug)]
struct CliIvol {
    /// Maximum error of root search
    #[clap(long, default_value = "1e-7")]
    maxerr: f64,
    /// Maximum number of root iterations
    #[clap(long, default_value = "1000000000")]
    maxiter: u32,
    /// Premium of option
    #[clap(short, long)]
    premium: f64,
}
fn main() {
    let cli = Cli::parse();
    if cli.verbosity > 1 {
        println!("Value for cli: {:#?}", cli);
    }
    // Get option data
    let option_type: OptionType = match cli.arg_option_type {
        ArgOptionType::Call => OptionType::Call,
        ArgOptionType::Put => OptionType::Put,
    };
    let s = cli.forward;
    let x: f64 = match cli.strike {
        Some(k) => k,
        None => cli.forward,
    };
    let time = cli.time;
    let tau = cli.tau;
    let r = cli.r;
    let b = cli.b;
    let sigma: f64;
    match &cli.command {
        Some(Commands::Ivol(cli_ivol)) => {
            sigma = tw_ivol(
                cli_ivol.premium,
                option_type,
                s,
                x,
                time,
                tau,
                r,
                b,
                cli_ivol.maxerr,
                cli_ivol.maxiter,
            )
            .unwrap()
        }
        None => sigma = cli.sigma,
    }
    let res = tw_option(option_type, s, x, time, tau, r, b, sigma);
    println!(
        "TW({},s={},x={},t={},tau={},r={},b={},sigma={}): {}",
        option_type, s, x, time, tau, r, b, sigma, res
    );
    println!(
        "Delta({},s={},x={},t={},tau={},r={},b={},sigma={}): {}",
        option_type,
        s + 1.0,
        x,
        time,
        tau,
        r,
        b,
        sigma,
        tw_option(option_type, s + 1.0, x, time, tau, r, b, sigma) - res,
    );
    println!(
        "StrikeDelta({},s={},x={},t={},tau={},r={},b={},sigma={}): {}",
        option_type,
        s,
        x + 1.0,
        time,
        tau,
        r,
        b,
        sigma,
        tw_option(option_type, s, x + 1.0, time, tau, r, b, sigma) - res,
    );
    let time1 = f64::max(time - 1.0 / 365.0, 0.0);
    let tau1 = f64::max(tau - 1.0 / 365.0, 0.0);
    println!(
        "DayTheta({},s={},x={},t={},tau={},r={},b={},sigma={}): {}",
        option_type,
        s,
        x,
        time1,
        tau1,
        r,
        b,
        sigma,
        tw_option(option_type, s, x, time1, tau1, r, b, sigma) - res,
    );
    println!(
        "Vega({},s={},x={},t={},tau={},r={},b={},sigma={}): {}",
        option_type,
        s,
        x,
        time,
        tau,
        r,
        b,
        sigma + 0.01,
        tw_option(option_type, s, x, time, tau, r, b, sigma + 0.01) - res,
    );
}
