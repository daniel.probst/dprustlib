use dprustlib::quantopts::*;
fn main() {
    println!("Hello Dan.");
    let s = 100.0f64;
    let x = 100.0f64;
    let tau = 1.0f64 / 360.0f64;
    let t = 3.0f64 / 360.0f64;
    let avg_days = 2;
    let sigma = 0.3f64;
    println!(
        "hhm_option(Call,{},-100.0,{},{},{},{},0,0.01,0.0,{}):{}",
        s,
        x,
        tau,
        t,
        avg_days,
        sigma,
        hhm_option(
            OptionType::Call,
            s,
            -100.0,
            x,
            tau,
            t,
            avg_days,
            0,
            0.01,
            0.0,
            sigma
        )
    );
}
